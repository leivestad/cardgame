package no.ntnu.idatg2001;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * HandOfCards class
 *
 * @Author Niklas Leivestad
 */
public class HandOfCards {

    private ArrayList<PlayingCard> hand = new ArrayList<>();

    /**
     * Constructor for a hand of cards, new ArrayList
     */
    public  HandOfCards() {
        this.hand = new ArrayList<>(hand);
    }

    public HandOfCards(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    public ArrayList<PlayingCard> getHand() {
        return hand;
    }

    public PlayingCard getCardAt(int index) {
        return hand.get(index);
    }

    public void addCard (PlayingCard newCard){

        hand.add(newCard);
    }

    public void addHand (ArrayList<PlayingCard> newHand){
        for (PlayingCard card : newHand){
            hand.add(card);
        }

    }

    /**
     * Clears the hand list
     */
    public void clearDeck () {
        hand.clear();
    }

    /**
     * @return Sums up cards on hand
     */
    public int sumOfCards() {
        return hand.stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * Get method for getting all the hearts in the hand.
     * @return List of hearts
     */
    public List<PlayingCard> getHearts(){
        return hand.stream().filter(playingCard -> playingCard.getSuit()== 'H').collect(Collectors.toList());
    }

    /**
     * checks hand for S13
     * @return boolean
     */
    public boolean queenOfSpades(){
        if(hand.stream().anyMatch(playingCard -> playingCard.getSuit()== 'S' && playingCard.getFace() == 12)){
            return true;
        }
            return false; //kan fjerne else

    }

    @Override
    public String toString() {
        return  hand +" ";
    }
}
